import { Util } from '../../../spec/util/Util';

export = async function getSeasonHeadToHead() {
    
    await this.Given(/^(.*) consume API service to get (.*) vs (.*) last (.*) results on the (.*) league$/, 
        async function (actor: string, team1: string, team2: string, lastMatches: number, league: string) {
            const results = await Util.getLastResultsBetweenTwoTeams(lastMatches,team1,team2,league);
            await console.log(results);
    });
}