Feature: Login on website
    As a Soccer sports open data user
    I want to get matches information between 2 team
    So that could decide which team is the best to bet

    @regression
    Scenario: Get Season head to head
    Given User consume API service to get Milan vs Inter last 5 results on the serie-a league
    When User get the API response data
    Then User should decide which team is the best to bet