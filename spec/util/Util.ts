import { ApiBase } from "../../api-services/apiBase";
import { SeasonHeadToHeadResponse } from "../../api-services/response-models/SeasonHeadToHeadResponse";

export class Util {

    public static async getLastResultsBetweenTwoTeams(numberOfMatches: number, homeTeam: string,
        awayTeam: string, league: string): Promise<number[]> {
        const results: number[] = [];
        let season = await this.getActualSeason();
        do {
            const response: SeasonHeadToHeadResponse = await ApiBase.getSeasonHeadToHead(
                league,season,homeTeam,awayTeam);
            for (const round of response.rounds){
                if (round.match_result != ''){
                    const winner = await this.getMatchWinner(
                        round.home_team,round.away_team,round.match_result);
                    if (winner == homeTeam){
                        results.push(RESULTS.HOME);
                    } else if(winner == awayTeam) {
                        results.push(RESULTS.AWAY)
                    } else {
                        results.push(RESULTS.TIE);
                    }
                }   
            }
            season = await this.getSpecificSeasonBefore(1,  season);
        } while (results.length < numberOfMatches);
        return results;
    }

    public static async getActualSeason(): Promise<string> {
        const today: Date =  new Date();
        const month: number = today.getMonth();
        let year: number = today.getFullYear();
        year = +(year.toString().substring(2,4));

        if(month <= 5) {
            return `${year-1}-${year}`;
        } else {
            return `${year}-${year+1}`;
        }
    }

    public static async getSpecificSeasonBefore(yearsBefore: number, actualSeason: string): Promise<string>{
        const seasonYears: string[] = actualSeason.split('-');
        return `${+seasonYears[0]-yearsBefore}-${+seasonYears[1]-yearsBefore}`;
    }

    public static async getMatchWinner(homeTeam: string, awayTeam: string, matchResult: string): Promise<string>{
        const scores: string[] = matchResult.split('-');
        if (+scores[0] > +scores[1]){
            return homeTeam;
        } else if (scores[0] < scores[1]){
            return awayTeam;
        } else {
            return 'tie';
        }
    }
}

export enum RESULTS {
    HOME = 0,
    TIE = 1,
    AWAY = 2
}