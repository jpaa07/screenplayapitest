export class BaseResponse {
    protected statusCode: string;
    protected errorCode: string;
    protected statusReason: string;
}