import * as request from 'request-promise';
import { Activity } from 'serenity-js/lib/screenplay';
import { SeasonHeadToHeadResponse } from './response-models/SeasonHeadToHeadResponse';

export class ApiBase {
    
    private static baseURL = `https://sportsop-soccer-sports-open-data-v1.p.mashape.com/v1`;

    public static async getSeasonHeadToHead (league: string, season: string, team1: string, team2: string): Promise<SeasonHeadToHeadResponse> {
        const url = `${this.baseURL}/leagues/${league}/seasons/${season}/rounds?team_1_slug=${team1}&team_2_slug=${team2}`
        const response =  await request({
			url: url,
			method: 'GET',
			headers: {
				'X-Mashape-Key': 'iJU4omqZbhmshS0dUys6im8AvhjNp13lYlAjsnjHIbIWP1tNN2',
				'Accept': 'application/json'
			},
			rejectUnauthorized: false,
			json: true
        });
        const data: SeasonHeadToHeadResponse = await response.data;
        console.log(data);
        return data;
    } 
        
    
}
